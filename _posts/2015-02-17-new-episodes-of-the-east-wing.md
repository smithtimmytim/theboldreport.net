---
layout: post
title: New Episodes of The East Wing
categories: links
custom_type: link
link_url: http://goodstuff.fm/theeastwing
date: '2015-02-17 12:01:24'
---
While I'm away in California, two awesome people agreed to guest host The East Wing for me, and they did an amazing job!

Last week, [Garth Braithwaite talked with Sean Martell](http://goodstuff.fm/theeastwing/9) about how he got started at Mozilla, how he designs in the open, mentors new designers, and how he works with the Mozilla community.

This week, [Liz Andrade talks with Nick Snyder](http://goodstuff.fm/theeastwing/10) about quitting your job, moving cross-country, and starting something new.

I love how much of their own personality they brought to the show. Thanks so much Garth and Liz!