---
layout: post
title: The Daily Radio Column
categories: links
custom_type: link
link_url: http://goodstuff.fm/dailycolumn
---

I started a new show. It's called The Daily Radio Column.

When I was fifteen, I started a small column that I would write everyday. "Column" is a fancy word for it, really. More accurately, it was me at my computer, with Microsoft Word open (thank goodness those days are gone), writing my thoughts on the world around me.

Recently, I found some of these, and naturally they're terrible. But it reminded me of the importance of practicing something, and how cathardic it can be to externalize your feelings on what's happening around you.

That is what I want this show to be, and I hope you'll join me for the ride.
