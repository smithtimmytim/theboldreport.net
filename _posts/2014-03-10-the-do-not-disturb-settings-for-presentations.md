---
layout: post
title: '‘The Do-Not-Disturb Settings For Presentations’'
categories: links
custom_type: link
link_url: http://simplicitybliss.com/blog/dnd-for-presentations
date: '2014-03-10 12:28:58'
---
I had just seen an app called [Hush](http://coffitivity.com/hush/) that turns off notifications on your Mac. It allows you to toggle notifications or specify a period of time in which to disable them. Turns out, it's baked into Mac OS X. As Sven points out, this is a great way in which Apple brought a feature in iOS, "back to the Mac."