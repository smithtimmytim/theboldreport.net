---
layout: post
title: 'The East Wing: Garth Braithwaite'
categories: links
custom_type: link
link_url: http://theeastwing.net/episodes/72
tags: ["The East Wing"]
date: '2013-10-08 16:53:12'
---
This week: I talk with Garth Braithwaite about how he got started, projects he’s worked on at Adobe, Open Source Design, and design education.

*Bandwidth is kindly provided by [Campaign Monitor](http://www.campaignmonitor.com/).*
