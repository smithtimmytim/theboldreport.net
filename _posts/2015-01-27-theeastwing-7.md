---
layout: post
title: 'The East Wing: Liz Andrade'
categories: links
custom_type: link
link_url: http://goodstuff.fm/theeastwing/7
date: '2015-01-27 16:14:01'
---
This week, I throw caution to the wind with Liz Andrade. We talk honestly about freelancing and running your own business. These days, it seems that everyone wants to sell you a course on how easy it is to work for yourself and make a lot of money. Sorry to burst your bubble, but it's a lie.

Take a listen and learn something that might actually be of help to you.

*Episode sponsored by [Campaign Monitor](https://www.campaignmonitor.com/).*