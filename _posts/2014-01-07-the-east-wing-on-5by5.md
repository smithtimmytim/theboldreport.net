---
layout: post
title: 'The East Wing on 5by5'
categories: articles
custom_type: post
date: '2014-01-07 13:33:29'
---
[The East Wing has moved to 5by5](http://5by5.tv/eastwing)! I'm really excited about the move, and I hope that the show will reach a lot more people. I want to thank [Dan Benjamin](http://5by5.tv/people/dan-benjamin) for inviting me to his network, and [Jory Raphael](http://sensibleworld.com/) for the gorgeous new artwork.

I've been working behind the scenes on this move for some time now, and I'm happy that the show is launched on it's new home.

### Listening Live
The East Wing will now record live at [5by5.tv/live](http://5by5.tv/live) every Wednesday at 3pm Central.

5by5 has some pretty awesome apps that you can use to listen as well. There's the [5by5 Radio app for iOS](https://itunes.apple.com/us/app/5by5-radio/id520847556?mt=8&partnerId=30&siteID=GfpxbBXXpXE-y3gfJGyOQcSr2tOpkzD12A&uo=8&at=11laDR) and [5by5 for Mac](https://itunes.apple.com/us/app/5by5-radio/id553498407?mt=12&partnerId=30&siteID=GfpxbBXXpXE-y3gfJGyOQcSr2tOpkzD12A&uo=8&at=11laDR). From there, you can decide to be notified when the show starts. As always, you can [subscribe to the Google calendar](http://theeastwing.net/schedule) which tells you the time and guest.

### Subscribing
You won't have to do a thing. I've redirected the feed to the new one on 5by5. If you're not currently subscribed, and you'd like to, the new feed is [feeds.5by5.tv/eastwing](feed://feeds.5by5.tv/eastwing)

### Thank You
I'll be recording a more in-depth state of the union for The East Wing later, but I just wanted to say thank you. Thank you for listening to the show, supporting the show by sharing it, reviewing it, and checking out the sponsors. It means a lot to me, and I hope you stick around for this new era of the show!