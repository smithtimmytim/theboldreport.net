---
layout: post
title: 'The East Wing: Liz Elcoate'
categories: links
custom_type: link
link_url: http://theeastwing.net/episodes/74
tags:
  - The East Wing
date: '2013-10-22 17:14:51'
---
This week: I talk with Liz Elcoate about her start in the design industry, her podcast, and how she’d never go back to a full-time job.

*Bandwidth sponsored by [Campaign Monitor](http://www.campaignmonitor.com/).*