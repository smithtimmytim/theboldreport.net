---
layout: post
title: "The East Wing: Dave Rupert"
categories: links
custom_type: link
link_url: http://goodstuff.fm/theeastwing/11
date: '2015-02-25 16:26:15'
---

This week, I talk with Dave Rupert about ShopTalk, the ever changing web industry, and what adjustments he's had to make personally. Change isn't as bad as we make it out to be.

*Sponsored by [Campaign Monitor](http://www.campaignmonitor.com/) and [listeners like you](http://patreon.com/ttimsmith).*
