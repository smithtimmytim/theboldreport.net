---
layout: post
title: Support Me on Patreon
categories: links
custom_type: link
link_url: https://www.patreon.com/ttimsmith
date: '2015-02-05 11:43:43'
---

Over a month ago, I published my very own Patreon page. Patreon allows me to continue working on the content that I love to produce, while giving readers and listeners the opportunity to directly support me.

As you most likely know, I would love to do podcasting and writing full-time. I know that's just not a realistic goal quite yet. However, as a reader or listener, you can still help me pay some bills, upgrade software, and even have the flexibility to create more shows.

If you'd like more information, how you can help, and what you get in return, [visit the Patreon page](https://www.patreon.com/ttimsmith). If you have further questions, don't hesitate to [email me](mailto:tim@theboldreport.net).

If you've been on the fence about supporting me, or this is the first time you hear about it, [please consider becoming a patron](https://www.patreon.com/ttimsmith). I would greatly appreciate it!