---
layout: post
title: 'Sponsor: Pencils.com'
categories: links
custom_type: link
link_url: http://syndicateads.net/s/vp
tags:
  - Sponsor
date: '2013-12-03 11:37:56'
---
At [Pencils.com](http://syndicateads.net/s/vp), we believe that creativity is the greatest of all virtues. And, with our selection of unique, high-quality pencils, notebooks, and creative tools, we've got everything you need to unleash yours. 

Whether you're a pencil nut who knows all the brands ([Caran d'Ache](http://www.pencils.com/all-brands/caran-d-ache?utm_source=syndicate&utm_medium=post&utm_content=caran&utm_campaign=dec), [Blackwing](http://www.pencils.com/all-brands/blackwing?utm_source=syndicate&utm_medium=post&utm_content=blackwing&utm_campaign=dec), [Faber-Castell](http://www.pencils.com/all-brands/faber-castell?utm_source=syndicate&utm_medium=post&utm_content=faber&utm_campaign=dec), we stock them all), or a casual doodler looking for something to inspire you, there's something for you on Pencils.com. Combine that with our legendary customer service and fast, reliable shipping, and you've got some serious creative potential.

So, go ahead and [read the story of the $40 pencil](http://www.pencils.com/brands/blackwing?utm_source=syndicate&utm_medium=post&utm_content=blackwing&utm_campaign=dec), learn about [the pencil company that has been around since the French Revolution](http://www.pencils.com/brands/faber-castell?utm_source=syndicate&utm_medium=post&utm_content=faber&utm_campaign=dec), and [find the perfect notebook to capture your ideas](http://www.pencils.com/journals-sketchbooks/luxury-journals?utm_source=syndicate&utm_medium=post&utm_content=journals&utm_campaign=dec). If you're in the giving mood, we also have gifts for [artists](http://www.pencils.com/gift-ideas/gifts-for-artists?utm_source=syndicate&utm_medium=post&utm_content=gifts-for-artists&utm_campaign=dec), [writers](http://www.pencils.com/gift-ideas/gifts-for-writers?utm_source=syndicate&utm_medium=post&utm_content=gifts-for-writers&utm_campaign=dec), [musicians](http://www.pencils.com/gift-ideas/gifts-for-musicians?utm_source=syndicate&utm_medium=post&utm_content=gifts-for-musicians&utm_campaign=dec), and [anyone else](http://www.pencils.com/gift-guide?utm_source=syndicate&utm_medium=post&utm_content=gift-guide&utm_campaign=dec) on your shopping list. 

Above all else, stay creative.

---

*A big thank you to [Pencils.com](http://syndicateads.net/s/vp) for sponsoring the RSS feed this week. [Sponsorship by The Syndicate.](http://syndicateads.net/)*
