---
layout: post
title: 'My Interview on The Shakes'
categories: links
custom_type: link
link_url: http://www.muleradio.net/theshakes/33/
tags:
  - Interview
date: '2013-12-20 12:14:24'
---
Pat and Jeremy were so kind to invite me on their show. We talk about how I started out, my age, Ron Burgundy, and a lot more.
