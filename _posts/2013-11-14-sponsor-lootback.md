---
layout: post
title: 'Sponsor: Lootback'
categories: links
custom_type: link
link_url: http://syndicateads.net/s/uj
tags:
  - Sponsor
date: '2013-11-14 12:50:24'
---
![Lootback ad](/uploads/2013/11/Lootback.png)

[According to recent research](http://www.executionists.com/blog/website-design/cost-to-build-websites-2013/), the average small business owner can expect to pay somewhere between $100 and $200 for stock images for their website. Of course, for the owner going through a professional designer, this is just part of a larger number. If you're a designer, you should always be looking for ways to bring down the final cost of a website. Outbidding the competition isn't the only factor when it comes to success, but cutting costs where possible certainly won't hurt. 

If you've been looking for a way to bring down your stock image costs and increase your bottom line, a new website may be able to help. It's called Lootback.com, and it functions as a stock image search engine. The site partners with some of the biggest names in the stock photo industry, including iStock, ShutterStock, Graphicriver, Themeforest, and more. The premise is simple: they get a commission on every photo you buy through their site and then split that commission with you. 

[Lootback](http://syndicateads.net/s/uj) provides users with a compilation image search engine. You type in whichever keywords fit your needs and it will come back with results from their industry partners. Helpfully, once you've created an account, Lootback will tell you right away how much you will save on a particular image once you've clicked on it. Rebates are paid into your account within 12 hours. That said, Lootback only pays out 4 times a year, so don't expect cash back right away. Still, if you're a designer who buys hundreds of images a year, the savings could prove substantial.

There are a lot of websites out there that promise to save the average shopper some money, but very few are dedicated to helping out web designers. Lootback aims to save designers time and money and they do a pretty good job of it. If you've been in search of a way to bring down your costs, [Lootback](http://syndicateads.net/s/uj) is a good place to start.

---

*Thanks to [Lootback](http://syndicateads.net/s/uj) for sponsoring the RSS feed this week. [Sponsorship by The Syndicate.](http://syndicateads.net/)*
