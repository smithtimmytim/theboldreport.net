---
layout: post
title: "Sponsor: Dot & Dash"
categories: links
custom_type: link
link_url: http://www.dotanddash.net/
date: '2015-02-23 12:59:44'
---
Dot & Dash is releasing it's first digital product called Simple Icons on March 2nd, 2015. [Visit the website](http://www.dotanddash.net/) to download a free version of the icon set. The full icon set will be available for purchase on March 2nd for $11. 

- **What is Dot & Dash?**     
Dot & Dash is a brand focused on designing quality digital and physical products and was created by [Erik Coon](http://twitter.com/eriktheviking).  

- **What file formats will I recieve when I purchase?**     
The Simple Icons set provides these file types:  Adobe Illustrator, EPS, PDF (for preview), and Photoshop. These are all vector based, and are infinitely scaleable. 

- **1-on-1 Support**     
If you have a problem purchasing or have a question about the product, you will get fast and friendly support from the creator of the icon set.

Use the code `simplicity` on March 2nd, and get $3 off your purchase.

---

*Thanks to [Dot & Dash](http://www.dotanddash.net/) for sponsoring The Bold report this week.*