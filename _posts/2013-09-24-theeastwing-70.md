---
layout: post
title: 'The East Wing: Jeremy Fuksa'
categories: links
custom_type: link
link_url: http://theeastwing.net/episodes/70
tags: ['The East Wing']
date: '2013-09-24 16:15:55'
---
This week: I talk with Jeremy Fuksa about being a design director, hiring designers, and the side-projects he works on.

*Bandwidth is kindly provided by [Campaign Monitor](http://www.campaignmonitor.com/).*