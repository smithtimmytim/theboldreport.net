---
layout: post
title: 'Facebook Removes ‘Hide from Search’ Privacy Setting'
categories: links
custom_type: link
link_url: http://bigstory.ap.org/article/facebook-no-longer-lets-users-hide-search
tags:
  - Facebook
date: '2013-10-11 12:27:27'
---
The *Associated Press* reports:

>Facebook Inc. said Thursday that it is removing a setting that controls whether users could be found when people type their name into the website's search bar.
>
>Facebook says only a single-digit percentage of the nearly 1.2 billion people on its network were using the setting.

What is Facebook without their continued unethical behavior involving privacy. And then to say "only a single-digit percentage of the nearly 1.2 billion people on its network" are using it? As [Sean Tubridy pointed out so well](https://twitter.com/tubes/status/388505400527450113), that's "between 10 million and 90 million people."

What Facebook seems to forget, is that what's bad for users, is bad for business. A lesson they might learn the hard way.