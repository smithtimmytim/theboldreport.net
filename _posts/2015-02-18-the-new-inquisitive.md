---
layout: post
title: The New Inquisitive
categories: links
custom_type: link
link_url: http://www.relay.fm/inquisitive/27
date: '2015-02-18 11:19:51'
---
Myke Hurley has rebooted his staple show, Inquisitive. For years, he's done some type of incarnation of this show, and interview after interview, Myke has always asked the right questions.

The new iteration of Inquisitive is absolutely fantastic. This show is an absolute delight in rhythm, in sound, and in editorial.

Here's [what Myke has to say](http://www.extras.relay.fm/blog/2015/2/18/inquisitive-behind-the-app) about the reboot:

> Making these shows has been an incredible amount of work, but I am so proud of them. It's totally different in style and production to anything I have made before, and I am learning new skills as I progress. I think I'm getting better at it every day too.
>
> I genuinely believe that my last five years in podcasting has taught me all of the base skills that I needed to do this, and 'Behind the App' is going to teach me what I need to know for the next five years.

It is my belief, that shows like [Inquisitive](http://www.relay.fm/inquisitive), [Reply All](http://gimletmedia.com/show/reply-all/), [99% Invisible](http://99percentinvisible.org/), and the unfortunately-on-hiatus [For the Record](http://goodstuff.fm/ftr), are the way of the future.