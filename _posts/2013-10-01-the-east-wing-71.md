---
layout: post
title: 'The East Wing: Scott Johnson'
categories: links
custom_type: link
link_url: http://theeastwing.net/episodes/71
tags: ["The East Wing"]
date: '2013-10-01 16:31:08'
---
This week: I talk with Scott Johnson about how he got started, takings risks, and being kind to people.

*Bandwidth is provided by [Campaign Monitor](http://www.campaignmonitor.com/).*