---
layout: post
title: 'Sponsor: Atlassian’s Agile Guide'
categories: links
custom_type: link
link_url: http://syndicateads.net/s/w3
tags:
  - Sponsor
date: '2013-12-10 10:25:43'
---
What's the point of an agile standup meeting? 

Gone are the days of 30-minute status meetings where most people are half-asleep or pecking away on their laptops, oblivious to what's being said. Agile standups are the leaner, more efficient cousin of status meetings where attendees actually stand up. On our feet, we're more focused, attentive, and concise. It's science!

Whether you need [robust tools](https://www.atlassian.com/agile/project-management?utm_source=bsa-syndicate&utm_medium=display&utm_content=standup-meetings&utm_campaign=agile-microsite#!standup-meeting) for planning and tracking projects, communicating with coworkers, deploying products, or just some [general tips](http://syndicateads.net/s/w3) on how to run an agile shop (and how to run them Rong?), Atlassian is here to offer you the tools and advice you need to get the most out of your agile practice. 

---

*A huge thanks to [Atlassian's Agile Guide](http://syndicateads.net/s/w3) for sponsoring the RSS feed this week. [Sponsorship by The Syndicate.](http://syndicateads.net/)*
