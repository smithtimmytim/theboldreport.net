---
layout: post
title: 'Sponsor: The Theme Foundry'
categories: links
custom_type: link
link_url: http://syndicateads.net/s/sl
date: '2013-10-01 11:32:29'
tags: Sponsor
---
![The Theme Foundry](http://syndicateads.net/cms/images/collections-theme-by-the-theme-foundry.jpg)

[The Theme Foundry](http://thethemefoundry.com/) has been building premium WordPress themes since 2008. They recently released Collections -- a unique and beautiful WordPress theme for sharing, designed by Veerle Pieters. Visit the [live demo of Collections](http://demo.thethemefoundry.com/collections-theme/) to see it in action, or [purchase it now for $79](http://thethemefoundry.com/wordpress/collections/).

**What makes The Theme Foundry special?**

- **A focus on quality over quantity.** You won't find a huge assortment on their site -- they keep a small, curated collection of [premium WordPress themes](http://thethemefoundry.com/wordpress/).

- **Exclusive partner with WordPress.com** (the official hosted WordPress provider). Each and every theme goes through a stringent audit process from some of the best WordPress coders in the world.

- **Whole team support.** You get fast and friendly support from the team that *actually built* your theme, not a part time support rep.

---
*Thanks to [The Theme Foundry](http://syndicateads.net/s/sl) for supporting The Bold Report this week. They're awesome people. You might also want to checkout [Memberful](http://memberful.com/), which is made by the same team.*

*[Sponsorship by The Syndicate.](http://syndicateads.net/)*
