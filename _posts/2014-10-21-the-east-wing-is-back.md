---
layout: post
title: 'The East Wing Is Back!'
categories: links
custom_type: link
link_url: http://goodstuff.fm/theeastwing/1
date: '2014-10-21 17:11:15'
---
My old show is back! I can't tell you how excited I am. This relaunch means that we're starting all over. We've started back from episode one!

This week, I talk with Guy Routledge about how he got his start, his video series AtoZ CSS, and a little behind-the-scenes work he does for the screencasts.

You can subscribe [via iTunes](https://itunes.apple.com/us/podcast/the-east-wing/id931108499?mt=2), or [via RSS](http://goodstuff.fm/theeastwing/feed). If you're still subscribed elsewhere, please update your subscription. If you're feeling especially kind, leave the show a rating or review in iTunes.

Thank you so much for listening and for your support!
