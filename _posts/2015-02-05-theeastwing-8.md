---
layout: post
title: 'The East Wing: Jonathan Cutrell'
categories: links
custom_type: link
link_url: http://goodstuff.fm/theeastwing/8
date: '2015-02-05 11:49:47'
---

This week, I talk with Jonathan Cutrell about how he got started, his new hit show Developer Tea, and the things no one wants to talk about when it comes to hiring and asking for money.

*Episode is sponsored by [Campaign Monitor](https://www.campaignmonitor.com/) and by my incredible patrons on [Patreon](https://www.patreon.com/ttimsmith).*