---
layout: post
title: 'Sponsor: Fracture. Your picture, directly on glass.'
categories: links
custom_type: link
link_url: http://syndicateads.net/s/vb
tags:
  - Sponsor
date: '2013-11-26 12:22:09'
---
[Fracture](http://fractureme.com/?utm_source=syndicate&utm_medium=post&utm_content=fracture&utm_campaign=syn2) prints your photo in vivid color directly on glass. It's a picture, frame, & mount all in one.

It's a [modern, elegant, and affordable way](http://fractureme.com/?utm_source=syndicate&utm_medium=post&utm_content=modern&utm_campaign=syn2) to print and display your favorite memories. Your print comes with everything you need to display your photo, right in the durable packaging.

Fractures come in a variety of [sizes and prices](http://fractureme.com/affordable-framing-prices?utm_source=syndicate&utm_medium=post&utm_content=sizes&utm_campaign=syn2), starting at just $12, with free shipping on orders of $100 or more.

Fracture prints make great gifts and are the perfect way to fill up empty walls in your new home or apartment. [Check it out.](http://fractureme.com/?utm_source=syndicate&utm_medium=post&utm_content=check&utm_campaign=syn2)

---

*Big thanks to [Fracture](http://syndicateads.net/s/vb) for sponsoring this week's RSS Feed. [Sponsorship by The Syndicate.](http://syndicateads.net/)*
