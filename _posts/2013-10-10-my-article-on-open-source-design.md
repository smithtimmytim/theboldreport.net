---
layout: post
title: 'Open Source Design: Using GitHub for Design Collaboration'
categories: links
custom_type: link
link_url: http://opensourcedesign.is/blogging_about/using-github-for-design-collaboration/
tags:
  - GitHub
  - Open Source Design
date: '2013-10-10 16:04:03'
---
I wrote a small article on how GitHub has really helped us on the Open Source Design project. With the use of GitHub issues, we've been able to talk and collaborate as a group. 