---
layout: post
title: 'The East Wing: Rachel Shillcock'
categories: links
custom_type: link
link_url: http://theeastwing.net/episodes/77
tags:
  - The East Wing
date: '2013-12-20 12:07:05'
---
This week: I talk with Rachel Shillcock about her speaking engagements, her new podcast, Beyond Ink, and an update on her freelancing.

*Sponsored by [Symbolicons](http://symbolicons.com/). Use the code 'timlovesicons' for 15% off.*