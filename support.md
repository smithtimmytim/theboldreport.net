---
title: "Support"
layout: page
exclude_from_search: true
permalink: /support/
---
Thank you for your interest in supporting *The Bold Report*! There are a couple of ways you can do this.

## Sponsorship
This is the perfect place to promote your product, or service to smart people interested in design, development, technology, Apple, geek culture, and more.

For more information on sponsorship availability and pricing on *The Bold Report*, [send me an email](mailto:tim@theboldreport.net).

## Patreon
If you're interested in supporting this site, as well as my other projects, you can do so via Patreon. Loyal readers like you help me pay bills, and dedicate more time to this site. For more information [visit my Patreon page](https://www.patreon.com/ttimsmith).