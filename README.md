# theboldreport.net

[![Code Climate](https://codeclimate.com/github/ttimsmith/theboldreport.net/badges/gpa.svg)](https://codeclimate.com/github/ttimsmith/theboldreport.net)

This is The Bold Report powered by [Jekyll](http://jekyllrb.com/). Feel free to browse the code and report any issues.

You're free to use whatever you'd like, **however**, posts found in the `_drafts` or `_posts` folder are copyright of Timothy B. Smith, and may not be used without proper attribution. Of course, the logo is also my copyright… you know what you can copy/paste and what you shouldn't. Be nice.

## Getting Started

    $ git clone git@github.com:ttimsmith/theboldreport.net.git
    $ cd theboldreport.net
    $ bundle install
    $ jekyll serve --watch

Open it up in <http://localhost:4000>, and voilà!
