---
layout: page
title: About
exclude_from_search: true
permalink: /about/
excerpt: Find out more about The Bold Report.
---

*The Bold Report* is designed, developed, and written by me, [Tim Smith](http://ttimsmith.com), in beautiful, and sometimes freezing, St. Paul, Minnesota. I'm a user interface and web designer, and I host [*For The Record*](http://towermedia.org/ftr), a podcast about people, life, and the experiences that make us who we are.

{% figure small__right /uploads/2013/09/tim_680x510.jpg "I was born with my eyebrows like that… kidding." %}

I'm a nerd who loves Star Wars and comic books, and a recovering coffee addict. When I'm not at my computer, I'm binge-watching shows, going to the movies, having brunch, or doing other old-married-couple things with my amazing wife, Kelly.

## Purpose
*The Bold Report* is my personal blog, and I write about a whole range of things including, but not limited to: design, development, technology, Geek culture, and Apple. This is *my personal blog*, so I write about whatever I find myself thinking about or reading. This isn't a "tech blog."

## Tools
Most writing is done on my Macbook Pro in [Byword](http://bywordapp.com/), but these days, I also write and publish from iOS in [Editorial](http://omz-software.com/editorial/). If you'd like to learn how I do that, [I wrote about it](/Publishing-To-Jekyll-From-iOS/). 

The site is designed and developed in [Sublime Text](http://www.sublimetext.com/), with the exception of the logo. That was done in Adobe Illustrator. I won't link to that one because if you've never heard of it, you've probably lived a better life. It's all hosted by [Heroku](https://www.heroku.com), and powered by [Jekyll](http://jekyllrb.com).

## Contact
If you have questions, concerns, feedback, or compliments about *The Bold Report*, please feel free to contact me. You can [reach me on Twitter](https://twitter.com/ttimsmith) or email [tim@theboldreport.net](mailto:tim@theboldreport.net).
